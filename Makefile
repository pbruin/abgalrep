# Makefile for Abelian (one-dimensional) Galois representations of Q

GP ?= gp -q

# characteristic bound
L := 17

# conductor bound
M := 32

all: characters

.PHONY: characters

characters: character-list.txt
	$(MAKE) `cat $<`

character-list.txt: Makefile
	echo "character_list($M, $L)" | $(GP) character-list.gp > $@

# Evaluate $(1) in GP, catching errors and writing the elements of
# the result (which must be a vector) to $(2), separated by newlines.
gp_eval = echo "iferr(apply(x->print(x),if(1," $(1) ")),e,write(\"/dev/stderr\",e);quit(1));" | $(GP) abgalrep.gp > $(2)

char_%.gp:
	$(call gp_eval,"abgalrep(`echo $* | sed -e 's/\(.*\)_\(.*\)_\(.*\)/znstar(\1, 1), \2, \3/'`)",$@)
