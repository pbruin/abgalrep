character_list(M, L) =
{
   my(D = chi, o);
   for(m = 1, M,
      D = znstar(m, 1);
      forprime(l = 1, L,
               if(valuation(m, l) < 2,
                  for(chi = 0, m - 1,
                     if(gcd(chi, m) == 1
                        && (o = charorder(D, chi)) % l != 0
                        && zncharconductor(D, chi) == m
                        && l^znorder(Mod(l, o)) <= L,
                        print(concat(["char_", Str(m), "_", Str(chi),
                                      "_", Str(l), ".gp"])))))));
}
