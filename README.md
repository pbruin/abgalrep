abgalrep
========

A program to compute (one-dimensional) Abelian representations of the
absolute Galois group of the rational field over finite fields.


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

A recent version of PARI/GP, <http://pari.math.u-bordeaux.fr/>.


Installation
------------

The program can be run from the source directory; no compilation or
installation is needed.  Edit the Makefile if necessary to adjust the
variable `GP` and to select which characters are computed by default.


Usage
-----

The function `abgalrep` takes three arguments:

- `znstar(n, 1)` (the unit group of the integers modulo `n`)

- `c` (the Conrey label of the character to compute)

- `l` (the characteristic of the finite field)

It can be called from PARI/GP as in the following example:

    gp> abgalrep(znstar(11, 1), 10, 3)
    %1 = [[x, x^2 + x + 3], [Mat(1), [1, 0; 0, 1]], [y, y^2 - y - 8], [Mat(1), [1, 0; 0, 1]], [1/3, 2/3, 1/3; 2/3, -2/3, -1/3; -1/3, 1/3, -16/3]]

Simply typing `make` in the source directory will compute all dual
pairs corresponding to characters of modulus at most 32 with values in
a finite field of characteristic at most 17.


TODO
----

- Implement scalar multiplication on the representation space for
  non-prime fields

- Speed up the computation using numerical approximation
